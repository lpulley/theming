# Configure Git
echo "Configuring git..."
${HOME}/theming/git/config.sh

# Install Zsh
echo "Installing zsh..."
sudo apt-get -yq install zsh neovim

# Link Zsh and Vim config to home
echo "Linking .zshrc..."
ln -s "${HOME}/theming/term/.zshrc" "${HOME}"
echo "Linking .vimrc..."
ln -s "${HOME}/theming/term/.vimrc" "${HOME}"
ln -s "${HOME}/.config/nvim/init.vim" "${HOME}"

# Change the user shell to zsh
echo "Changing user shell..."
chsh -s /usr/bin/zsh

# Clone Zgen
echo "Installing zgen..."
git clone https://github.com/tarjoilija/zgen.git "${HOME}/.zgen"

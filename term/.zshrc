# load zgen
source "${HOME}/.zgen/zgen.zsh"
ZGEN_RESET_ON_CHANGE=(${HOME}/.zshrc)
ZSH_PYENV_LAZY_VIRTUALENV=true

SPACESHIP_DOCKER_SHOW=false

# if the init script doesn't exist
if ! zgen saved; then
    zgen load zsh-users/zsh-syntax-highlighting
    zgen load zsh-users/zsh-history-substring-search
    zgen load unixorn/autoupdate-zgen
    zgen load hcgraf/zsh-sudo
    zgen load thetic/extract
    zgen load davidparsson/zsh-pyenv-lazy
    zgen load denysdovhan/spaceship-prompt spaceship

    #chmod -R 755 "${HOME}/.zgen"
    zgen save
fi

bindkey '^[OA' history-substring-search-up
bindkey '^[OB' history-substring-search-down
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

HYPHEN_INSENSITIVE="true"

prompt_context(){}
alias note='vim ~/notes.md'
alias vim='vim -p'
alias sapt='sudo apt'
alias sudo='sudo '

alias ls='ls --color=auto'
alias l='ls -lahF'
alias ll='ls -lhF'
alias grep='grep --color=auto'

alias gcmt='git commit'
alias gpsh='git push'
alias gpul='git pull'
alias gfch='git fetch'
alias gchk='git checkout'
alias gadd='git add'
alias glog='git log --graph --decorate --oneline'
alias gsts='git status --short'
alias gssh='git stash'
alias gcln='git clone'
alias gsub='git submodule'
alias gmrg='git merge'
alias gbrh='git branch'

alias dc='docker-compose'
alias dps='docker ps --format "table {{.Names}}\t{{.Image}}\t{{.RunningFor}}\t{{.Status}}" -a'

alias chdef='chmod -R a=r,u+w,a+X'

alias findhere="find . -name"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
alias mkdir="mkdir -p"
alias code.="code .; exit"
alias code+="code -a .; exit"

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
bindkey -e
# End of lines configured by zsh-newuser-install

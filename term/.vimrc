set wrap
set scrolloff=5
set backspace=indent,eol,start
set ttyfast
set laststatus=2
set showmode
set number
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%l,%v][%p%%]\ [BUFFER=%n]\ %{strftime('%c')}
set encoding=utf-8
set hlsearch
set viminfo='100,<9999,s100

syntax on
colorscheme elflord

nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf

autocmd BufWinLeave ?* mkview
autocmd BufWinEnter ?* silent loadview
